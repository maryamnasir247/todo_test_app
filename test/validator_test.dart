import 'package:flutter_test/flutter_test.dart';
import '../lib/todo_validator.dart';

void main() {
  test('Title cannot be empty', () {
    var error = TodoValidator().getTitleFieldError("Learn Flutter");
    expect(error, "");
  });

  // This test will fail.
  // test('Title cannot be empty', () {
  //   var error = TodoValidator().getTitleFieldError("");
  //   expect(error, "");
  // });

  test('Priority cannot be empty', () {
    var error = TodoValidator().getPriorityFieldError("");
    expect(error, "Priority is required");
  });

  // Grouping the above two unit tests
  group('Todo Validator', () {
    test('Title cannot be empty', () {
      var error = TodoValidator().getTitleFieldError("Learn Flutter");
      expect(error, "");
    });

    test('Priority cannot be empty', () {
      var error = TodoValidator().getPriorityFieldError("Medium");
      expect(error, "");
    });
  });
}
