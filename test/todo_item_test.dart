import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import '../lib/main.dart';
import '../lib/todo.dart';
import '../lib/todo_item.dart';

void main() {
  testWidgets('Todo item must have title and priority',
      (WidgetTester tester) async {
    var todo = Todo(
      id: 't1',
      title: 'Learn Flutter',
      priority: 'Medium',
      isCompleted: false,
    );

    markCompleted(value) {
      todo.isCompleted = value;
    }

    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: TodoItem(todo, markCompleted),
        ),
      ),
    );
    final titleFinder = find.text('Learn Flutter');
    final priorityFinder = find.text('Medium');

    expect(titleFinder, findsOneWidget);
    expect(priorityFinder, findsOneWidget);
    // Test will fail when following lines of code are un-commented.
    // expect(titleFinder, findsNothing);
    // expect(priorityFinder, findsNothing);
  });

  testWidgets('Pressing floating action button opens up Add Todo modal',
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());

    await tester.tap(find.byType(FloatingActionButton));

    await tester.pumpAndSettle();

    expect(find.widgetWithText(ElevatedButton, 'Add Todo'), findsOneWidget);
    // Test will fail when any of the following lines of code are un-commented.
    // expect(find.widgetWithText(RaisedButton, 'Add Todo'), findsNothing);
    // expect(find.widgetWithText(RaisedButton, 'Add Todos'), findsOneWidget);
  });
}
