# todo_test_app

Flutter app for managing your todos and PoC for Unit and Widget Testing.

## Getting Started

- Run flutter test test/validator_test.dart to run unit tests.
- Run flutter test test/todo_item_test.dart to run widget tests.

Few resources to get started with unit testing and widget testing:

- Unit Testing (https://flutter.dev/docs/cookbook/testing/unit/introduction)
- Widget Testing (https://flutter.dev/docs/cookbook/testing/widget/introduction)

