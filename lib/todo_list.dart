import 'package:flutter/material.dart';
import './todo_item.dart';
import './todo.dart';

class TodoList extends StatelessWidget {
  final List<Todo> _todoList;
  final Function _markAsCompleted;

  TodoList(this._todoList, this._markAsCompleted);

  @override
  Widget build(BuildContext context) {
    return _todoList.isEmpty
        ? Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('No todo items!'),
              ],
            ),
          )
        : ListView.builder(
            itemCount: _todoList.length,
            itemBuilder: (BuildContext ctx, int index) {
              return TodoItem(
                _todoList[index],
                (value) => _markAsCompleted(index, value),
              );
            },
          );
  }
}
