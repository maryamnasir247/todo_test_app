import 'package:flutter/material.dart';
import './todo.dart';

class TodoItem extends StatelessWidget {
  final Todo todoItem;
  final Function _markAsCompleted;

  TodoItem(this.todoItem, this._markAsCompleted);

  Color getColor(priority) {
    if (priority == "Low") {
      return Colors.green;
    } else if (priority == "Medium") {
      return Color(0xfff5b642);
    } else {
      return Colors.red;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      margin: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 8,
      ),
      child: ListTile(
        leading: Checkbox(
          value: todoItem.isCompleted,
          onChanged: (bool? value) {
            if (value == true) {
              _markAsCompleted(value);
            }
          },
        ),
        title: Text(
          todoItem.title,
          style: TextStyle(
            decoration: todoItem.isCompleted
                ? TextDecoration.lineThrough
                : TextDecoration.none,
          ),
        ),
        subtitle: Text(
          todoItem.priority,
          style: TextStyle(color: getColor(todoItem.priority)),
        ),
      ),
    );
  }
}
