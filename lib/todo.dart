class Todo {
  String id;
  String title;
  String priority;
  bool isCompleted;

  Todo({
    required this.id,
    required this.title,
    required this.priority,
    required this.isCompleted,
  });
}
