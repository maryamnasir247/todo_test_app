import 'package:flutter/material.dart';
import './todo_validator.dart';

class AddTodo extends StatefulWidget {
  final Function _addTodoItem;

  AddTodo(this._addTodoItem);

  @override
  _AddTodoState createState() => _AddTodoState();
}

class _AddTodoState extends State<AddTodo> {
  final titleController = TextEditingController();
  var _priority = "Low";
  var _todoValidator = TodoValidator();
  var titleError = null;

  void _onPriorityChange(value) {
    setState(() {
      _priority = value;
    });
  }

  void submitData() {
    final titleInput = titleController.text;
    var titleErrorMessage = _todoValidator.getTitleFieldError(titleInput);
    setState(() {
      titleError = titleErrorMessage == "" ? null : titleErrorMessage;
    });
    if (titleErrorMessage != "") return;

    widget._addTodoItem(titleInput, _priority);

    Navigator.of(context).pop();
  }

  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              controller: titleController,
              decoration: InputDecoration(
                labelText: 'Title',
                errorText: titleError,
              ),
            ),
            SizedBox(
              height: 24,
            ),
            Container(
              width: double.infinity,
              child: Text('Priority'),
            ),
            SizedBox(
              height: 8,
            ),
            Row(
              children: [
                Radio(
                  value: "Low",
                  groupValue: _priority,
                  onChanged: (value) => _onPriorityChange(value),
                ),
                Text('Low'),
                SizedBox(
                  width: 10,
                ),
                Radio(
                  value: "Medium",
                  groupValue: _priority,
                  onChanged: (value) => _onPriorityChange(value),
                ),
                Text('Medium'),
                SizedBox(
                  width: 10,
                ),
                Radio(
                  value: "High",
                  groupValue: _priority,
                  onChanged: (value) => _onPriorityChange(value),
                ),
                Text('High'),
              ],
            ),
            SizedBox(
              height: 24,
            ),
            ElevatedButton(
              child: Text('Add Todo'),
              onPressed: submitData,
            ),
          ],
        ),
      ),
    );
  }
}
