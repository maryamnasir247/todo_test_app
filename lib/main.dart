import 'package:flutter/material.dart';
import './todo_list.dart';
import './todo.dart';
import './add_todo.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Todo List',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Todo List'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Todo> _todoList = [];

  void _markAsCompleted(int index, bool value) {
    var todo = Todo(
      id: _todoList[index].id,
      title: _todoList[index].title,
      isCompleted: value,
      priority: _todoList[index].priority,
    );
    setState(() {
      _todoList.removeAt(index);
      _todoList.add(todo);
    });
  }

  void _addTodoItem(String title, String priority) {
    var todo = Todo(
      id: DateTime.now().toString(),
      title: title,
      priority: priority,
      isCompleted: false,
    );
    setState(() {
      _todoList.insert(0, todo);
    });
  }

  void _openNewTodoModal(context) {
    showModalBottomSheet(
      context: context,
      builder: (_) {
        return AddTodo(_addTodoItem);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: TodoList(_todoList, _markAsCompleted),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => _openNewTodoModal(context),
      ),
    );
  }
}
