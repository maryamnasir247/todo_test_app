class TodoValidator {
  String getTitleFieldError(title) {
    return title.isEmpty ? "Title field is required" : "";
  }

  String getPriorityFieldError(priority) {
    return priority.isEmpty ? "Priority is required" : "";
  }
}
